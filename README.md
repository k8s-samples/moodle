 moodle in kubernetes with CEPH PVC
====================================

### Observations
* namespace: kube-public


## Create the secrets
```
$ kubectl apply -f secret-moodle.yaml
secret "moodle" created
```

## Deploy MariaDB

### Creating Persistent Volumes (CEPH)
```
$ kubectl apply -f cephfs-pv-db.yaml
persistentvolume "moodle-db" created

$ kubectl apply -f cephfs-pvc-db.yaml
persistentvolumeclaim "moodle-db" created

$ kubectl -n kube-public get pvc moodle-db
NAME        STATUS    VOLUME      CAPACITY   ACCESS MODES   STORAGECLASS   AGE
moodle-db   Bound     moodle-db   20Gi       RWX                           1m
```

### Creating mariadb pod
```
$ kubectl apply -f deploy-mariaddb.yaml
deployment.extensions "moodle-db" created

$ kubectl -n kube-public get pod -l app=moodle
NAME                         READY     STATUS    RESTARTS   AGE
moodle-db-7c747fd985-rhk96   1/1       Running   0          3m

```

### Create mariadb svc
```
$ kubectl apply -f svc-mariadb.yaml
service "moodle-db" created
```

## Go to Moodle!

### Creating Persistent Volumes (CEPH)

```
$ kubectl apply -f cephfs-pv-app.yaml
persistentvolume "moodle-app" created

$ kubectl apply -f cephfs-pvc-app.yaml 
persistentvolumeclaim "moodle-app" created

$ kubectl -n kube-public get pvc -l app=moodle
NAME         STATUS    VOLUME       CAPACITY   ACCESS MODES   STORAGECLASS   AGE
moodle-app   Bound     moodle-app   20Gi       RWX                           1m
moodle-db    Bound     moodle-db    20Gi       RWX                           35m
```

### Creating moodle pod
```
$ kubectl apply -f deploy-moodle.yaml
deployment.extensions "moodle-app" created
```

### Creating moodle svc
```
kubectl apply -f svc-moodle.yaml
service "moodle-app" created
```

### Creating Ingress
```
kubectl apply -f ingress.yaml
ingress.extensions "moodle-app" created
```